const express = require('express');
const router = express.Router();

const { removeById } = require('../services/user.service');
const { isAuthorized } = require("../middlewares/auth.middleware");

router.delete('/:id', isAuthorized, (req, res) => {
    const result = removeById(req.params.id);
    if (typeof result === 'object') {
        return res.status(200).json({ message: 'Successful user deletion' });
    }
    res.status(404).json({ errors: result });
});

module.exports = router;