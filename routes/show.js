const express = require('express');
const router = express.Router();

const { findById } = require('../services/user.service');

router.get('/:id', (req, res) => {
    const result = findById(req.params.id);
    if ( typeof result === 'object') {
        return res.status(200).json({ data:result });
    }
    res.status(404).json({ errors: result });
});

module.exports = router;
