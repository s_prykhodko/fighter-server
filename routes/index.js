const express = require('express');
const router = express.Router();

const { userlist } = require("../services/data.provider");

router.get('/', (req, res) => res.status(200).json({ data: userlist }));

module.exports = router;
