const express = require('express');
const router = express.Router();

const { check, validationResult } = require('express-validator/check');
const { saveData } = require("../repositories/user.repository");
const { isAuthorized } = require("../middlewares/auth.middleware");

router.post('/', [
  check('name')
    .isAlpha().withMessage('Must be only alphabetical chars')
    .isLength({ min: 1 }).withMessage('Must be at least 1 char long'),
  check('health').isNumeric().withMessage('Must be only digits')
    .custom(value => parseInt(value) > 0).withMessage('Must be greater then 0'),
  check('attack').isNumeric().withMessage('Must be only digits')
    .custom(value => parseInt(value) > 0).withMessage('Must be greater then 0'),
  check('defense').isNumeric().withMessage('Must be only digits')
    .custom(value => parseInt(value) > 0).withMessage('Must be greater then 0'),
  check('source').isURL().withMessage('Must be only URL format')
  ], isAuthorized, (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    const result = saveData(req.body);
    res.status(200).json({ message: 'Successful user creation', data: result });
});

module.exports = router;