const express = require('express');
const router = express.Router();

const { isAuthorized } = require("../middlewares/auth.middleware");
const { updateById } = require('../services/user.service');
const { check, validationResult } = require('express-validator/check');

router.put('/:id',[
    check('name')
        .isAlpha().withMessage('Must be only alphabetical chars')
        .isLength({ min: 1 }).withMessage('Must be at least 1 char long')
        .optional(),
    check('health')
        .isNumeric().withMessage('Must be only digits')
        .custom(value => typeof value === 'number').withMessage('Must be a number')
        .custom(value => value > 0).withMessage('Must be greater then 0')
        .optional(),
    check('attack')
        .isNumeric().withMessage('Must be only digits')
        .custom(value => typeof value === 'number').withMessage('Must be a number')
        .custom(value => value > 0).withMessage('Must be greater then 0')
        .optional(),
    check('defense')
        .isNumeric().withMessage('Must be only digits')
        .custom(value => typeof value === 'number').withMessage('Must be a number')
        .custom(value => value > 0).withMessage('Must be greater then 0')
        .optional(),
    check('source')
        .isURL().withMessage('Must be only URL format')
        .optional()
    ], isAuthorized, (req, res) => {

        const result = updateById(req.params.id, req.body);
        if (typeof result === 'object') {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
              return res.status(422).json({ errors: errors.array() });
            }
            return res.status(200).json({ message: 'Successful user updating', data: result });
        }
        res.status(404).json({ errors: result });
});

module.exports = router;