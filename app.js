const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const createRouter = require('./routes/create');
const showRouter = require('./routes/show');
const updateRouter = require('./routes/update');
const deleteRouter = require('./routes/delete');
const invalidRouter = require('./routes/invalid');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/users', indexRouter);
app.use('/', indexRouter);
app.use('/users', createRouter);
app.use('/users', showRouter);
app.use('/users', updateRouter);
app.use('/users', deleteRouter);
app.use('/', invalidRouter);

module.exports = app;
