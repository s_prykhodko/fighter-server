const { userlist } = require("../services/data.provider");

const findById = id => {
    const filteredList = userlist.filter(user => user._id == id);
    return filteredList.length > 0 ? filteredList[0] : `No user with ${id} id`;
};

const removeById = id => {
    const isIdExisted = findById(id);
    if (typeof isIdExisted === 'object') {
        userlist.splice(userlist.indexOf(isIdExisted), 1);
        return userlist; 
    } else {
        return isIdExisted;
    }
}

const updateById = (id, body) => {
    const userExisted = findById(id);
    if (typeof userExisted === 'object') {
            Object.keys(userExisted).forEach(key => {
            if (key !== '_id' && body[key]) {
                userExisted[key] = body[key];
            }
        });
        return userExisted;
    } else {
        return userExisted;
    }
};

const findMaxId = () => {
    let maxId = 0;
    userlist.forEach(user => {
        const id = parseInt(user._id);
        if ( id > maxId) {
            maxId = id;
        }
    });
    return maxId;
}

const addId = (data) => {
   const currentId = findMaxId() + 1;
   data._id = currentId.toString();
   return data;
};

module.exports = {
    findById,
    removeById,
    updateById,
    addId
};