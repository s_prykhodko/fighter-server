const { userlist } = require("../services/data.provider");
const { addId } = require('../services/user.service');

const saveData = data => {
    data = addId(data);
    const correctData = {};

    correctData._id = data._id;
    correctData.name = data.name;
    correctData.health = parseInt(data.health);
    correctData.attack = parseInt(data.attack);
    correctData.defense = parseInt(data.defense);
    correctData.source = data.source;

    userlist.push(correctData);
    return correctData;
}

module.exports = {
    saveData
};